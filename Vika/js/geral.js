
	$(function(){

		// SCRIPTS HEIGHT 100%
		$('.alturaAutomatica').css({'height':($(document).height()-122)+'px'});
		$(window).resize(function(){
			$('.alturaAutomatica').css({'height':($(document).height()-122)+'px'});
		});
		$('.pg-tutorial,.pg-404').css({'height':($(document).height())+'px'});
		$(window).resize(function(){
			$('.pg-tutorial,.pg-404').css({'height':($(document).height())+'px'});
		});

		// SCRIPT FORMULÁRIO DE PESQUISA POR #TAG @USUÁRIO
		$(".opcoesPesquisa").children("li").click(function(e){
			$(".pesquisarbtn").val($(this).attr("data-parametro"));
			$(".pesquisarbtn").text($(this).attr("data-parametro"));
			$(".campoText").attr("placeholder",$(this).text());
			if ($(this).attr("data-parametro") != "@"){
				$(".resultadosPesquisa").addClass('tag');
			}else{
				$(".resultadosPesquisa").removeClass('tag');
			}
		});
		$(".campoText").keyup(function(){
			if ($(this).val() != "") {
				$(".resultadosPesquisa").slideDown();
				$(".opcoesPesquisa").hide();
			}else if($(this).val() == ""){
				$(".resultadosPesquisa").slideUp();
				$(".opcoesPesquisa").show();
			}
		});

		// SCRIPT MODAL
		// setTimeout(function(){
		//   $(".pg-tutorial").fadeIn("slow");
		// }, 2000);
		// $(".pg-tutorial").click(function(e) {
		//    $(".pg-tutorial").fadeOut("slow");
		// });

		

	});
